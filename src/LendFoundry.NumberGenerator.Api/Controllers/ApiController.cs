﻿using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator.Api.Controllers
{
    /// <summary>
    /// Represents the api controller class.
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApiController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public ApiController(IGeneratorService service) { Service = service; }

        private IGeneratorService Service { get; }

        /// <summary>
        /// Generates the number.
        /// </summary>
        /// <param name="entityid">The entityid.</param>
        /// <returns></returns>
        [HttpPost("{entityid}")]
        [ProducesResponseType(typeof(NextNumber), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GenerateNumber(string entityid)
        {
            return await ExecuteAsync(async () => Ok(new NextNumber(entityid, await Service.TakeNext(entityid))));
        }

        /// <summary>
        /// Generates the nested number.
        /// </summary>
        /// <param name="entityid">The entityid.</param>
        /// <param name="subEntityIds">The sub entityIds under which new number will be generated</param>
        /// <returns></returns>
        [HttpPost("{entityid}/{*subEntityIds}")]
        [ProducesResponseType(typeof(NextNumber), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GenerateNestedNumber(string entityid, string subEntityIds)
        {
            List<string> subEntityIdsList = null;
            if (!string.IsNullOrWhiteSpace(subEntityIds))
            {
                subEntityIds = WebUtility.UrlDecode(subEntityIds);
                subEntityIdsList = SplitTags(subEntityIds);
            }

            return await ExecuteAsync(async () => Ok(new NextNumber(entityid, await Service.TakeNext(entityid, subEntityIdsList))));
        }
        private static List<string> SplitTags(string tags)
        {
            return string.IsNullOrWhiteSpace(tags)
                ? new List<string>()
                : tags.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}