﻿namespace LendFoundry.NumberGenerator
{
	public interface IConfigType
	{
		int LeftPadding { get; set; }
		string Prefix { get; set; }
		string Separator { get; set; }
		int StartsFrom { get; set; }
		string Suffix { get; set; }
		NumberType Type { get; set; }
	}
}