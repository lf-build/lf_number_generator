﻿namespace LendFoundry.NumberGenerator
{
	public class ConfigType 
	{
		public string Prefix { get; set; }
		public string Suffix { get; set; }
		public NumberType Type { get; set; }
		public string Separator { get; set; }
        public int StartsFrom { get; set; }
		public int LeftPadding { get; set; }
	}
}