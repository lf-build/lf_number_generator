﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.NumberGenerator
{
    public interface IConfiguration : IDependencyConfiguration
    {
		IDictionary<string, ConfigType> Configurations { get; set; }
    }
}
