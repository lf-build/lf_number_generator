﻿namespace LendFoundry.NumberGenerator
{
	public class NextNumber
	{
		public NextNumber()
		{
		}

		public NextNumber(string entityId, string number)
		{
			EntityId = entityId;
			Number = number;
		}

		public string EntityId { get; set; } 
		public string Number { get; set; } 
	}
}