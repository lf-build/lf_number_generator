﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator
{
    public interface IGenerator
    {
        Task<string> GenerateIt(string entityId);
        Task<string> GenerateItAsync(string entityId, List<string> subEntityId);
    }
}
