﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.NumberGenerator
{
    public class Number : Aggregate, INumber
    {
        public string EntityId { get; set; }
        public int LatestNumber { get; set; }

    }
}
