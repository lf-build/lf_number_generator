﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.Framework.DependencyInjection;
#endif


namespace LendFoundry.NumberGenerator.Client
{
    public static class NumberGeneratorServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddNumberGeneratorService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddSingleton<INumberGeneratorServiceFactory>(p => new NumberGeneratorServiceFactory(p, endpoint, port));
            services.AddSingleton(p => p.GetService<INumberGeneratorServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddNumberGeneratorService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<INumberGeneratorServiceFactory>(p => new NumberGeneratorServiceFactory(p, uri));
            services.AddSingleton(p => p.GetService<INumberGeneratorServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddNumberGeneratorService(this IServiceCollection services)
        {
            services.AddSingleton<INumberGeneratorServiceFactory>(p => new NumberGeneratorServiceFactory(p));
            services.AddSingleton(p => p.GetService<INumberGeneratorServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

       
    }
}
