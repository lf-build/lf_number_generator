﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using System.Threading.Tasks;
using MongoDB.Driver;
using System;

namespace LendFoundry.NumberGenerator.Persistence
{
    public class NumberRepository : MongoRepository<INumber, Number>, INumberRepository
    {
        static NumberRepository()
        {
            BsonClassMap.RegisterClassMap<Number>(map =>
            {
                map.AutoMap();
                var type = typeof(Number);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public NumberRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "numbers")
        {
            CreateIndexIfNotExists("primary-key", Builders<INumber>.IndexKeys.Ascending(u => u.TenantId).Ascending(u => u.EntityId),true);
        }

        public async Task<int> TakeNextNumber(string entityId, int startsFrom)
        {
            var tenantId = TenantService.Current.Id;
            if (startsFrom <= 0)
            {
                var result = await Collection.FindOneAndUpdateAsync(s =>
                        s.TenantId == tenantId &&
                        s.EntityId == entityId,
                        new UpdateDefinitionBuilder<INumber>()
                            .Inc(f => f.LatestNumber, 1)
                        , new FindOneAndUpdateOptions<INumber, Number>() { IsUpsert = true, ReturnDocument = ReturnDocument.After });
                return result.LatestNumber;
            }
            else
            {
                //NOTE: When we have startsFrom, We need to do insert/update, tried with .Inc and .SetOnInsert function but not working

                //Note: Try to update, it will return null if it is not updated.
                var result = await Collection.FindOneAndUpdateAsync(s =>
                        s.TenantId == tenantId &&
                        s.EntityId == entityId,
                new UpdateDefinitionBuilder<INumber>()
                    .Inc(f => f.LatestNumber, 1)
                , new FindOneAndUpdateOptions<INumber, Number>() { IsUpsert = false, ReturnDocument = ReturnDocument.After });

                if (result == null)
                {
                    result = new Number()
                    {
                        EntityId = entityId,
                        LatestNumber = startsFrom,
                        TenantId = tenantId
                    };

                    //Note: Try to insert
                    try
                    {
                        await Collection.InsertOneAsync(result);
                    }
                    catch (MongoWriteException)
                    {
                        //Note: If write fail then might be inserted by other thread
                        return await TakeNextNumber(entityId, startsFrom);
                    }
                }
                return result.LatestNumber;
            }

        }
    }
}
