﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator
{
    public class IncrementalGenerator : IGenerator
    {
        public IncrementalGenerator
        (
            INumberGeneratorRepository repository,
            INumberRepository numberRepository,
            ConfigType config,
            ITenantTime tenantTime
        )
        {
            Repository = repository;
            Config = config;
            TenantTime = tenantTime;
            NumberRepository = numberRepository;
        }

        private INumberGeneratorRepository Repository { get; }
        private INumberRepository NumberRepository { get; }

        private ConfigType Config { get; }

        private ITenantTime TenantTime { get; }

        public async Task<string> GenerateIt(string entityId)
        {
            return await Task.Run(async () =>
            {
                var startsFrom = 0;
                if (Config.StartsFrom > 1)
                    startsFrom = Config.StartsFrom;

                var nextNumber = await NumberRepository.TakeNextNumber(entityId, startsFrom);

                // in case of manual seed initialization
                if (nextNumber == 0 && Config.StartsFrom > 0)
                    nextNumber = Config.StartsFrom;

                // get next number incremented
                var next = (nextNumber).ToString();

                // padding verification
                if (Config.LeftPadding > 0)
                    next = next.PadLeft(Config.LeftPadding, '0');

                var generatedNumber = $"{Config.Prefix?.Trim()}{Config.Separator?.Trim()}{next}";
                if (!string.IsNullOrWhiteSpace(Config.Suffix))
                    generatedNumber += $"{Config.Separator?.Trim()}{Config.Suffix?.Trim()}";

                var detail = new NumberDetail
                {
                    EntityId = entityId,
                    GeneratedAt = new TimeBucket(TenantTime.Today),
                    GeneratedNumber = generatedNumber,
                    Number = next,
                    Prefix = Config.Prefix,
                    Separator = Config.Separator,
                    Suffix = Config.Suffix,
                    Type = NumberType.Incremental
                };
                Repository.Add(detail);
                return detail.GeneratedNumber;
            });
        }

        public async Task<string> GenerateItAsync(string entityId, List<string> subEntityIds)
        {
            return await GenerateIt(entityId + "_" + string.Join("_", subEntityIds));
        }
    }
}
