﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator.Tests.Fakes
{
    public class FakeNumberRepository : INumberRepository
    {
        public ConcurrentDictionary<string, int> InMemoryData { get; set; } = new ConcurrentDictionary<string, int>();

        public Task<int> TakeNextNumber(string entityId, int startsFrom)
        {
            if (startsFrom == 0)
                startsFrom = 1;
            return Task.FromResult(InMemoryData.AddOrUpdate(entityId, (startsFrom), (key, oldValue) => oldValue + 1));
        }
    }
}
